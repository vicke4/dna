from urllib import request
from os import path
import sqlite3

# Establishing connection with db
conn = sqlite3.connect('../db/dna.sqlite')
cursor = conn.cursor()

# Downloads the file from web
resp = request.urlopen('https://opensnp.org/data/8193.23andme.6530')

# Decodes the byte data to string
txt = resp.read().decode('utf-8')

# Splitting with new line to read line by line
txt_list = txt.strip().split('\n')

# Creating dna table to store data
cursor.execute('DROP TABLE IF EXISTS snp')
cursor.execute(
    '''
        CREATE TABLE snp (
            rsid varchar(15),
            chromosome varchar(2),
            position int,
            genotype varchar(2)
        )
    '''
)

# creating rows in snp table
for line in txt_list:
    # ignores commented part of the file
    if line[0] == '#':
        continue

    row_tuple = tuple(line.strip().split('\t'))
    print(row_tuple)
    cursor.execute('INSERT INTO snp VALUES (?, ?, ?, ?)', row_tuple)

conn.commit()

# closing connection
cursor.close()
conn.close()
