const express = require('express');

const router = express.Router();

const { dbConnection } = require('../config');

/* GET home page. */
router.get('/', async (req, res) => {
  const db = await dbConnection;
  const query = `
    SELECT chromosome, COUNT(*) as variants
    FROM snp GROUP BY chromosome;
  `;
  const { isFinite } = Number;
  const sortFunc = ({ chromosome: aChrom }, { chromosome: bChrome }) => {
    let first = parseInt(aChrom, 10) || (aChrom === 'X' ? 23 : aChrom);
    let second = parseInt(bChrome, 10) || (bChrome === 'X' ? 23 : bChrome);
    if (!isFinite(first)) first = first === 'Y' ? 24 : 25;
    if (!isFinite(second)) second = second === 'Y' ? 24 : 25;

    if (first < second) { return -1; }
    if (first > second) { return 1; }
    return 0;
  };

  const dbResp = await db.all(query);
  const chromosomeVariants = dbResp.sort(sortFunc);
  res.render('index', { chromosomeVariants });
});

module.exports = router;
