# Chromosome & Variants

<p align="middle">
    <img src="https://gitlab.com/vicke4/dna/raw/master/public/images/screenshot.png" />
</p>

# How to run
```
git clone https://gitlab.com/vicke4/dna.git
cd dna && npm install
npm run start
```

Now the app must be running at localhost:3000

# Tech Stack
 - Backend (Node.js & express)
 - Db (sqlite - to eliminate the pain of db configuration of the cloners)
 - Frontend (pug templates - as the problem is small)
