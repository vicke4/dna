const sqlite = require('sqlite');
const Promise = require('bluebird');
const path = require('path');

const dbPath = path.resolve(__dirname, '../db/dna.sqlite');

module.exports = sqlite.open(dbPath, { Promise });
